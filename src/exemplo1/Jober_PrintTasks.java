/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;

/**
 *
 * @author Lucio
 */
public class Jober_PrintTasks implements Runnable {

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
    private static long contador = 0;
    private final long identificador;

    public Jober_PrintTasks(String name){
        contador++;
        identificador = contador;
        taskName = name;
        
        //Tempo aleatorio entre 0 e 1 segundo
        if(identificador % 2 == 0) {
            sleepTime = 0;
        }else{
            sleepTime = generator.nextInt(1000); //milissegundos
        }
    }
    
    public void run(){
        try{
            System.out.printf("Tarefa "+identificador+": %s dorme por %d ms\n", taskName, sleepTime);
            //Estado de ESPERA SINCRONIZADA
            //Nesse ponto, a thread perde o processador, e permite que
            //outra thread execute
            Thread.sleep(sleepTime);
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
